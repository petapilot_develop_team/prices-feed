package com.petapilot.workshop.bean;

import com.petapilot.workshop.domain.Price;

import java.sql.Date;

public class PriceToSqlBean {

    public String toSql(Price price) {
        return "insert into price_value " +
                "(product_id, description, date, value) values (" +
                "'" + price.getProductId() + "', " +
                "'" + price.getDescription() + "', " +
                "'" + new Date(price.getDate().getTime()) + "', " +
                "'" + price.getValue() + "') ";
    }
}
