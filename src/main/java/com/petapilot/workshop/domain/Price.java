package com.petapilot.workshop.domain;

import java.math.BigDecimal;
import java.util.Date;

public class Price {

    private Integer priceId;
    private Integer productId;
    private String description;
    private Date date;
    private BigDecimal value;

    public Price() {
    }

    public Integer getPriceId() {
        return priceId;
    }

    public void setPriceId(Integer priceId) {
        this.priceId = priceId;
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public BigDecimal getValue() {
        return value;
    }

    public void setValue(BigDecimal value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "Price{" + "productId=" + productId +
                ", description='" + description + '\'' +
                ", date=" + date +
                ", value=" + value +
                '}';
    }
}

