package com.petapilot.workshop.route;

import com.petapilot.workshop.bean.PriceToSqlBean;
import com.petapilot.workshop.domain.Price;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.jdbc.JdbcComponent;
import org.apache.camel.component.properties.PropertiesComponent;
import org.apache.commons.dbcp.BasicDataSource;

/**
 * A Camel Java DSL Router
 */
public class PriceFeedRouteBuilder extends RouteBuilder {

    /**
     * Let's configure the Camel routing rules using Java code...
     */
    public void configure() throws Exception {
        loadApplicationProperties();
        configureDataSource();

        // here is a sample which processes the input files
        // (leaving them in place - see the 'noop' flag)
        from("file:src/data?noop=true")
                .unmarshal().jacksonxml(Price.class)
                .bean(PriceToSqlBean.class)
                .to("jdbc:pricesDataSource");
    }

    private void loadApplicationProperties() {
        getContext().getComponent("properties", PropertiesComponent.class)
                .setLocation("classpath:application.properties");
    }

    private void configureDataSource() throws Exception {
        BasicDataSource ds = new BasicDataSource();
        ds.setDriverClassName("com.mysql.jdbc.Driver");
        ds.setUrl(getContext().resolvePropertyPlaceholders("{{datasource.url}}"));
        ds.setUsername(getContext().resolvePropertyPlaceholders("{{datasource.username}}"));
        ds.setPassword(getContext().resolvePropertyPlaceholders("{{datasource.password}}"));
        getContext().getComponent("jdbc", JdbcComponent.class).setDataSource(ds);
    }
}
