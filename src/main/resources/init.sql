create table if not exists price_value (
    price_id INT AUTO_INCREMENT PRIMARY KEY;
    product_id INT,
    description VARCHAR(255) NOT NULL,
    date DATE,
    value DECIMAL(20,4)
);